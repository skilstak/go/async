package async

import "sync"

// Job must be fulfilled to send jobs to a WorkGroup. Note that the
// implementation of the Job() method must be completely safe for concurrency.
type Job interface {
	Job()
}

// WorkGroup encapsulates a channel of Jobs with one or more goroutine and
// sync.WaitGroup for them all. See NewWorkGroup().
type WorkGroup struct {
	Jobs chan Job
	wait sync.WaitGroup
}

// Close closes the Jobs channel and waits for all the goroutines to finish.
func (g *WorkGroup) Close() {
	close(g.Jobs)
	g.wait.Wait()
}

// NewWorkGroup creates an WorkGroup with count number of goroutines all
// concurrently reading from the same Jobs channel for work to do. To send the
// WorkGroup an Job send anything that implements the Job interface
// to the Jobs channel (ex: wg.Jobs <- MyAsyncJob{}). Note that this will block
// until the WorkGroup has a goroutine freed up to work on it. To avoid
// blocking put the channel send itself into its own goroutine.
func NewWorkGroup(count int) *WorkGroup {
	g := new(WorkGroup)
	g.Jobs = make(chan Job)
	g.wait.Add(count)
	for i := 0; i < count; i++ {
		go func() {
			for job := range g.Jobs {
				job.Job()
			}
		}()
		g.wait.Done()
	}
	return g
}
