package async_test

import (
	"fmt"
	"runtime"
	"sync"
	"time"

	"gitlab.com/skilstak/go/async"
)

// define a struct
type aJob struct {
	node string
}

// fulfill the Job interface
func (j aJob) Job() {
	fmt.Println("Would do job for " + j.node)
	time.Sleep(2 * time.Second)
}

func Example() {

	// setup the different job parameters

	var nodes = []string{
		"hello-world",
		"about",
		"go",
		"gophers",
		"concurrency",
	}

	// create a workgroup with one goroutine for every CPU
	g := async.NewWorkGroup(runtime.NumCPU())

	// setup as many input job routines as needed
	var wg sync.WaitGroup
	wg.Add(len(nodes))

	for _, node := range nodes {
		j := aJob{node: node}
		go func() {
			g.Jobs <- j
			wg.Done()
		}()
	}
	wg.Wait()
	g.Close()
}
